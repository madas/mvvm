package com.projects.mvvm.adapters;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.squareup.picasso.Picasso;

public class BindingAdapterTool {

    @BindingAdapter({"setImageUrl"})
    public void setImageUrl(ImageView imageView, String url) {
        Picasso.get()
                .load(url)
                .centerCrop()
                .fit()
                .into(imageView);
    }
}
