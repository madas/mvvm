package com.projects.mvvm.networks.impelementations;

import com.android.volley.Response;

import org.json.JSONArray;

public interface IVolleyAPI {
    void getUserList(Response.Listener<JSONArray> successListener, Response.ErrorListener errorListener);
}
