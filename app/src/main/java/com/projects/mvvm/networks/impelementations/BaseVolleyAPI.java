package com.projects.mvvm.networks.impelementations;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.projects.mvvm.AppConfiguration;
import com.projects.mvvm.BuildConfig;
import com.projects.mvvm.helpers.JSONHelper;
import com.projects.mvvm.networks.VolleyJSONObjectRequest;
import com.projects.mvvm.networks.VolleySingleton;
import com.projects.mvvm.networks.requests.BaseRequest;

import org.json.JSONArray;
import org.json.JSONObject;

public abstract class BaseVolleyAPI implements IVolleyAPI {

    protected Context context;

    public BaseVolleyAPI(Context context) {
        this.context = context;
    }

    protected String getApiUrl(String routeName) {
        JSONObject object = AppConfiguration.getOurInstance().getApiUrl(context);
        return BuildConfig.BASE_URL + JSONHelper.getOurInstance().readString(object, routeName);
    }

    protected void executeJSONObjectRequest(
            String url,
            int method,
            BaseRequest requestParameter,
            Response.Listener<JSONObject> successListener,
            Response.ErrorListener errorListener) {

        requestParameter.setUrl(url);
        requestParameter.setMethod(method);
        JSONObject jsonParameter = requestParameter.generateJSONParameter();
        VolleyJSONObjectRequest request = new VolleyJSONObjectRequest(requestParameter.getMethod(), url, jsonParameter, successListener, errorListener);
        VolleySingleton.getOurInstance().addToRequestQueue(request, context);
    }

    protected void executeJSONArrayRequest(
            String url,
            Response.Listener<JSONArray> successListener,
            Response.ErrorListener errorListener) {

        JsonArrayRequest request = new JsonArrayRequest(url, successListener, errorListener);
        VolleySingleton.getOurInstance().addToRequestQueue(request, context);
    }

}