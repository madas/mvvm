package com.projects.mvvm.networks.impelementations;

import android.content.Context;

import com.android.volley.Response;

import org.json.JSONArray;

public class VolleyAPI extends BaseVolleyAPI {

    public VolleyAPI(Context context) {
        super(context);
    }

    @Override
    public void getUserList(Response.Listener<JSONArray> successListener, Response.ErrorListener errorListener) {
        String url = getApiUrl("getUsers");
        executeJSONArrayRequest(url, successListener, errorListener);
    }
}
