package com.projects.mvvm.models;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class UserModel extends BaseModel {

    @SerializedName("login")
    private String userName;
    @SerializedName("avatar_url")
    private String avatarUrl;
    @SerializedName("html_url")
    private String repositoryUrl;
    @SerializedName("type")
    private String type;

    @Override
    protected JSONObject getAsJSONObject() {
        return null;
    }
}
