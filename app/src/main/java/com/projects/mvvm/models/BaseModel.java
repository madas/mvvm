package com.projects.mvvm.models;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class BaseModel {

    protected abstract JSONObject getAsJSONObject();

    protected String readString(JSONObject resource, String attributeKey) {
        if (resource.has(attributeKey)) {
            try {
                return resource.getString(attributeKey);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    protected Integer readInteger(JSONObject resource, String attributeKey) {
        if (resource.has(attributeKey)) {
            try {
                return resource.getInt(attributeKey);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return 0;
    }

    protected Boolean readBoolean(JSONObject resource, String attributeKey) {
        if (resource.has(attributeKey)) {
            try {
                return resource.getBoolean(attributeKey);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}
