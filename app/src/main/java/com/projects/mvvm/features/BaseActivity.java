package com.projects.mvvm.features;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

public abstract class BaseActivity<Binding extends ViewDataBinding> extends AppCompatActivity {

    protected Binding binding;
    protected abstract int attachLayout();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, attachLayout());
        initData();
        initLayout();
        initAction();
    }

    protected void initData() {

    }

    protected void initLayout() {

    }

    protected void initAction() {

    }
}
