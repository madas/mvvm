package com.projects.mvvm.features.home;

import com.projects.mvvm.R;
import com.projects.mvvm.databinding.ActivityHomeBinding;
import com.projects.mvvm.features.BaseActivity;

public class HomeActivity extends BaseActivity<ActivityHomeBinding> {

    @Override
    protected int attachLayout() {
        return R.layout.activity_home;
    }

    @Override
    protected void initData() {
        super.initData();
    }
}